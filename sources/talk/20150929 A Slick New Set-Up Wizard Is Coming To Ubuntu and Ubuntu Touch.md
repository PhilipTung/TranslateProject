A Slick New Set-Up Wizard Is Coming To Ubuntu and Ubuntu Touch
================================================================================
> Canonical aims to 'seduce and reassure' those unfamiliar with the OS by making a good first impression

**The Ubuntu installer is set to undergo a dramatic makeover.**

Ubuntu will modernise its out-of-the-box experience (OOBE) to be easier and quicker to complete, look more ‘seductive’ to new users, and better present the Ubuntu brand through its design.

Ubiquity, the current Ubuntu installer, has largely remained unchanged since its [introduction back in 2010][1].

### First Impressions Are Everything ###

Since the first thing most users see when trying Ubuntu for the first time is an installer (or set-up wizard, depending on device) the design team feel it’s “one of the most important categories of software usability”.

“It essentially says how easy your software is to use, as well as introducing the user into your brand through visual design and tone of voice, which can convey familiarity and trust within your product.”

Canonical’s new OOBE designs show a striking departure from the current look of the Ubiquity installer used by the Ubuntu desktop, and presents a refined approach to the way mobile users ‘set up’ a new Ubuntu Phone.

![Old design (left) and the new proposed design](http://www.omgubuntu.co.uk/wp-content/uploads/2015/09/desktop-2.jpg)

Old design (left) and the new proposed design

Detailing the designs in [new blog post][2], the Canonical Design team say the aim of the revamp is to create a consistent out-of-the-box experience across Ubuntu devices.

To do this it groups together “common first experiences found on the mobile, tablet and desktop” and unifies the steps and screens between each, something they say moves the OS closer to “achieving a seamless convergent platform.”

![New Ubuntu installer on desktop/tablet (left) and phone](http://www.omgubuntu.co.uk/wp-content/uploads/2015/09/Convergence.jpg)

New Ubuntu installer on desktop/tablet (left) and phone

Implementation of the new ‘OOBE’ has already begun’ according to Canonical, though as of writing there’s no firm word on when a revamped installer may land on either desktop or phone images.

With the march to ‘desktop’ convergence now in full swing, and a(nother) stack of design changes set to hit the mobile build in lieu of the first Ubuntu Phone that ‘transforms’ in to a PC, chances are you won’t have to wait too long to try it out.

**What do you think of the designs? How would you go about improving the Ubuntu set-up experience? Let us know in the comments below.**

--------------------------------------------------------------------------------

via: http://www.omgubuntu.co.uk/2015/09/new-look-ubuntu-installer-coming-soon

作者：[Joey-Elijah Sneddon][a]
译者：[译者ID](https://github.com/译者ID)
校对：[校对者ID](https://github.com/校对者ID)

本文由 [LCTT](https://github.com/LCTT/TranslateProject) 原创编译，[Linux中国](https://linux.cn/) 荣誉推出

[a]:https://plus.google.com/117485690627814051450/?rel=author
[1]:http://www.omgubuntu.co.uk/2010/09/ubuntu-10-10s-installer-slideshow-oozes-class
[2]:http://design.canonical.com/wp-content/uploads/Convergence.jpg