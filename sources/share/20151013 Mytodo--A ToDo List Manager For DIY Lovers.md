alim0x translating

Mytodo: A ToDo List Manager For DIY Lovers
================================================================================
![](http://itsfoss.itsfoss.netdna-cdn.com/wp-content/uploads/2015/10/Mytodo-Linux.jpg)

Usually, I focus on applications that are hassle free and easy to use (read GUI oriented). That’s the reason why I included [Go For It][1] to do program in the list of [Linux productivity tools][2]. Today, I am going to show you yet another to-do list application that is slightly different than the rest.

[Mytodo][3] is an open source to-do list program that puts you, the user, in command of everything. Unlike most other similar programs, Mytodo is more DIY hobbyist oriented because it lets you configure the server (if you want to use it on multiple computers), provides a command line interface among other main features.

It is written in Python and thus it could be used in all Linux distros and other operating systems such as Windows.

Some of the main features of Mytodo are:

- Both GUI and command line interface
- Configure your own server
- Add users/password
- Written in Python
- Searchable by tag
- To-do list list can be displayed as [Conky][4]

![](http://itsfoss.itsfoss.netdna-cdn.com/wp-content/uploads/2015/10/Mytodo-list.jpeg)

GUI

![](http://itsfoss.itsfoss.netdna-cdn.com/wp-content/uploads/2015/10/Mytodo-list-cli.jpeg)

Command Line

![](http://itsfoss.itsfoss.netdna-cdn.com/wp-content/uploads/2015/10/Mytodo-list-conky.jpeg)

Conky displaying to-do list

You can find the source code and configuration instructions on the Github link below:

- [Download and Configure Mytodo ][5]

While some people might not like all this command line and configuration part, but it certainly has its own pleasure. I let you try and decide if Mytodo suits our need and taste.

Image credit: https://pixabay.com/en/to-do-list-task-list-notes-written-734587

--------------------------------------------------------------------------------

via: http://itsfoss.com/mytodo-list-manager/

作者：[Abhishek][a]
译者：[译者ID](https://github.com/译者ID)
校对：[校对者ID](https://github.com/校对者ID)

本文由 [LCTT](https://github.com/LCTT/TranslateProject) 原创编译，[Linux中国](https://linux.cn/) 荣誉推出

[a]:http://itsfoss.com/author/abhishek/
[1]:http://itsfoss.com/go-for-it-to-do-app-in-linux/
[2]:http://itsfoss.com/productivity-tips-ubuntu/
[3]:https://github.com/mohamed-aziz/mytodo
[4]:http://itsfoss.com/conky-gui-ubuntu-1304/
[5]:https://github.com/mohamed-aziz/mytodo
